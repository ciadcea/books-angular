FROM trion/ng-cli

USER root
WORKDIR /angular/
COPY ./ /angular/

RUN npm install

ENV ANGULAR_PORT 4200
ENV BACKEND_HOST "localhost"
ENV BACKEND_PORT 3000

EXPOSE ${ANGULAR_PORT}

CMD sed -i "s/__BACKEND_HOST__/${BACKEND_HOST}/" src/environments/environment.docker.ts && \
    sed -i "s/__BACKEND_PORT__/${BACKEND_PORT}/" src/environments/environment.docker.ts && \
    ng serve --configuration docker --host 0.0.0.0 --port ${ANGULAR_PORT} --disable-host-check

export class Book {
    _id: string;
    title: string;
    author: string;
    description: string;
    image: string; // Base64

    constructor(title: string, author: string, description: string, image: string) {
        this.title = title;
        this.author = author;
        this.description = description;
        this.image = image;
    }

    static from(book: Book): Book {
        return new Book(
            book.title,
            book.author,
            book.description,
            book.image
        );
    }

    static equals(book1: Book, book2: Book): boolean {
        return book1.title === book2.title &&
            book1.author === book2.author &&
            book1.description === book2.description &&
            book1.image === book2.image;
    }
}

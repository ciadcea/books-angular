import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Book } from '../models/book';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(
    private http: HttpClient
  ) { }

  getBooks(): Observable<Book> {
    return this.http.get<Book>(`http://${environment.backendHost}:${environment.backendPort}/books`);
  }

  addBook(book: Book): Observable<Book> {
    return this.http.post<Book>(`http://${environment.backendHost}:${environment.backendPort}/book`, book);
  }

  getBook(id: string): Observable<Book> {
    return this.http.get<Book>(`http://${environment.backendHost}:${environment.backendPort}/book/${id}`);
  }

  putBook(id: string, book: Book): Observable<Book> {
    return this.http.put<Book>(`http://${environment.backendHost}:${environment.backendPort}/book/${id}`, book);
  }

  deleteBook(id: string): Observable<Book> {
    return this.http.delete<Book>(`http://${environment.backendHost}:${environment.backendPort}/book/${id}`);
  }
}

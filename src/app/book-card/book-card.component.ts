import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { Book } from '../models/book';

@Component({
  selector: 'app-book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.css']
})
export class BookCardComponent implements OnInit {

  @Input() book: Book;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
  }

  alert(message: string) {
    this.router.navigate(['/book', this.book._id]);
  }
}

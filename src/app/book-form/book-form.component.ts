import { Component, OnInit, Input, ViewChild } from '@angular/core';

import { Book } from '../models/book';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @ViewChild('image', { static: false })
  image;

  @Input()
  book = new Book('', '', '', '');

  constructor() {
  }

  ngOnInit() {
  }

  onImageSelected(selected) {
    const fileReader = new FileReader();
    fileReader.onload = loaded => {
      this.book.image = String(fileReader.result);
      this.image.nativeElement.src = fileReader;
    };

    fileReader.readAsDataURL(selected.target.files[0]);
  }

  deleteImage() {
    this.book.image = '';
    this.image.nativeElement.src = '../../assets/book.png';
  }
}

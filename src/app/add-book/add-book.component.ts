import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { BookFormComponent } from '../book-form/book-form.component';
import { Book } from '../models/book';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})
export class AddBookComponent implements OnInit {

  @ViewChild(BookFormComponent, { static: false })
  bookForm: BookFormComponent;

  // reference to initially empty book for later change detection if user leaves page
  emptyBook = new Book('', '', '', '');

  constructor(
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
  }

  goBack() {
    const editedBook = this.bookForm.book;

    if (Book.equals(this.emptyBook, editedBook)) {
      this.router.navigate(['/books']);
    } else {
      const discard = confirm('Are you sure you want to leave? All changes will be discarded.');
      if (discard) {
        this.router.navigate(['/books']);
      }
    }
  }

  addBook() {
    this.bookService.addBook(this.bookForm.book).subscribe(
      () => this.router.navigate(['/books']),
      error => console.error(error)
    );
  }
}

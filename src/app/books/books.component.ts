import { Component } from '@angular/core';

import { Observable } from 'rxjs';

import { Book } from '../models/book';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent {

  booksObservable: Observable<Book>;

  constructor(
    private bookService: BookService
  ) {
    this.booksObservable = this.bookService.getBooks();
  }
}

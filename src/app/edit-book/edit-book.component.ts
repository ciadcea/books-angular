import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { BookFormComponent } from '../book-form/book-form.component';
import { Book } from '../models/book';
import { BookService } from '../services/book.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})
export class EditBookComponent implements OnInit {

  @ViewChild(BookFormComponent, { static: false })
  bookForm: BookFormComponent;

  bookId: string;

  // reference to initial book state for later change detection if user leaves page
  initialBook: Book;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private bookService: BookService
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.bookId = params.get('bookId');

      this.bookService.getBook(this.bookId).subscribe(
        book => {
          this.initialBook = Book.from(book);
          this.bookForm.book = book;
        },
        error => console.error(error)
      );
    });
  }

  goBack() {
    const editedBook = this.bookForm.book;

    if (Book.equals(this.initialBook, editedBook)) {
      this.router.navigate(['/books']);
    } else {
      const discard = confirm('Are you sure you want to leave? All changes will be discarded.');
      if (discard) {
        this.router.navigate(['/books']);
      }
    }
  }

  deleteBook() {
    this.bookService.deleteBook(this.bookId).subscribe(
      () => this.router.navigate(['/books']),
      error => console.error(error)
    );
  }

  updateBook() {
    const updatedBook = this.bookForm.book;

    this.bookService.putBook(this.bookId, updatedBook).subscribe(
      () => this.router.navigate(['/books']),
      error => console.error(error)
    );
  }
}

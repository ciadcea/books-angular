export const environment = {
  production: true,

  backendHost: 'books-backend',
  backendPort: 3000
};
